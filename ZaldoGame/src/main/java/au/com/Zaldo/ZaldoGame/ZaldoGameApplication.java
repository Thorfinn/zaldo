package au.com.Zaldo.ZaldoGame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZaldoGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZaldoGameApplication.class, args);
	}

}
