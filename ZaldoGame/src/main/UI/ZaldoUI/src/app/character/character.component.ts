import { Component, OnInit, Input } from '@angular/core';

//movement locks
var lockedD = false;
var lockedA = false;
var lockedSpace = false;
//jumping
var jumpVel = 150;
var jumptick = 0;
var increasedHeight=0;
var originalHeight = 0;
//timing
var intervalID = null;
var runInterval = null;
var slowDownInterval = null;
var flag;

//running
var maxSpeed = 17;
var currentSpeed = 0
var runtick = 0;
var clockspeed = 10; //ms - global game speed
var runningDirection = '';
var map = {}; 

onkeyup = function(up){
  up || event; // to deal with IE
  map[up.code] = up.type == 'keyup';
  //logKey(ed)
  if ((up.code == 'KeyD') || up.code == 'ArrowRight') {
    window.clearInterval(runInterval);
    runInterval = null;
    runtick = 0;
    //slow down movement till stopped
    if (slowDownInterval == null){
      slowDownInterval = setInterval(slowDownInRightDirection, clockspeed);
    }
  }
  
  if ((up.code == 'KeyA') || (up.code == 'ArrowLeft')) {
    window.clearInterval(runInterval);
    runInterval = null;
    runtick = 0;
    //slow down movement till stopped
  if (slowDownInterval == null ) {
      slowDownInterval =  setInterval(slowDownInRightDirection, clockspeed);
  }
 }
  if ((up.code == 'KeyQ') || (up.code == 'ArrowLeft')) {
  //   window.clearInterval(runInterval);
  //   runInterval = null;
  //   runtick = 0;
  //   //slow down movement till stopped
  // if (slowDownInterval == null ) {
  //     slowDownInterval =  setInterval(slowDownInRightDirection, clockspeed);
  // }
  step(performance.now());
  }
}

onkeydown = function(down){
  down || event; // to deal with IE
    map[down.code] = down.type == 'keydown';
    //logKey(ed)
    if ((down.code == 'KeyD') || (down.code == 'ArrowRight')) {
        window.clearInterval(slowDownInterval);
        slowDownInterval = null;
        runtick = 0;
      if (runInterval == null ) {
          runningDirection = 'right';
          runInterval =  setInterval(runInRightDirection, clockspeed);
      }
    }

     if ((down.code == 'KeyA') || (down.code == 'ArrowLeft')) {
        window.clearInterval(slowDownInterval);
        slowDownInterval = null;
        runtick = 0;
      if (runInterval == null ) {
          runningDirection = 'left';
          runInterval =  setInterval(runInRightDirection, clockspeed);
      }
     }
    

    if ((down.code == 'Space' ||down.code == 'ArrowUp') && !lockedSpace) {
      //move char to right.
      //document.getElementById('Player').style.left = (parseInt(document.getElementById('Player').style.left)  + 15).valueOf() + "px";
      //alert('asdasd');
      //setTimeout(jump, 300)
      console.log('space');
      jumptick = 0;
      increasedHeight = 0;
      originalHeight = parseInt(document.getElementById('Player').style.top);
      lockedSpace = true;
      flag = true;    //speed = distance / time... so for a time period 300 ms,  s = 15 / 300   =  v = 15px  per 300 milliseconds 0.05px/ms
      intervalManager(true);
    }
    
    function jump() {
      jumptick = jumptick + 1;
      // lock left and right since airborne
      //lockedA = true;
      //lockedD = true;
      lockedSpace = true;
      //parabolic like jump needed (px(t) - 1/2 (9.8) t^2) 
      increasedHeight = (((jumpVel * jumptick) - (5 * jumptick*jumptick))/100)
      document.getElementById('Player').style.top = ((parseInt(document.getElementById('Player').style.top))  - increasedHeight).valueOf() + "px";
      if (originalHeight - 1 < parseInt(document.getElementById('Player').style.top)) {
  
        document.getElementById('Player').style.top = originalHeight.toString()  + "px";
        lockedSpace = false;
        lockedA = false;
        lockedD = false;
        clearInterval(intervalID);
        intervalManager(false);
        flag = false;
      }
    }


   


    function intervalManager(flag) {
      if(flag)
        intervalID =  window.setInterval(jump, clockspeed);
      else
      
      window.clearInterval(intervalID);
    }

    
    function movmentManager(flag) {
      if(flag)
        intervalID =  window.setInterval(jump, clockspeed);
      else
      window.clearInterval(intervalID);
    }

}

var start = null;
var element = document.getElementById('Player');
//window.requestAnimationFrame(step);

function step(timestamp) {
  if (!start) start = timestamp;
  var progress = timestamp - start;
  document.getElementById('Player').style.transform = 'translateX(' + Math.min(progress / 10, 2000) + 'px)';
  if (progress < 20000) {
    console.log(progress);
    window.requestAnimationFrame(step);
  }
}


function slowDownInRightDirection() {
  console.log(runtick + " " + currentSpeed + " " + maxSpeed);
  runtick = runtick - 0.2;
  // current speed could be a value whatever it is... current speed will increase based on number ot 'ticks' dureatino held
  currentSpeed =  currentSpeed + runtick;
  if (currentSpeed <= 0) {
    currentSpeed = 0;
    window.clearInterval(slowDownInterval);
  }
  if (runningDirection == 'left') {
    document.getElementById('Player').style.left = (parseInt(document.getElementById('Player').style.left)  - currentSpeed).valueOf() + "px";
  } else if (runningDirection == 'right') {
    document.getElementById('Player').style.left = (parseInt(document.getElementById('Player').style.left)  + currentSpeed).valueOf() + "px";
  } 
 
}

function runInRightDirection() {
  console.log(runtick + " " + currentSpeed + " " + maxSpeed);
  runtick = runtick + 0.2;
  // current speed could be a value whatever it is... current speed will increase based on number ot 'ticks' dureatino held
  currentSpeed =  currentSpeed + runtick;
  if (currentSpeed >= maxSpeed) {
    currentSpeed = maxSpeed;
  }
  if (runningDirection == 'left') {
    document.getElementById('Player').style.left = (parseInt(document.getElementById('Player').style.left)  - currentSpeed).valueOf() + "px";
  } else if (runningDirection == 'right') {
    document.getElementById('Player').style.left = (parseInt(document.getElementById('Player').style.left)  + currentSpeed).valueOf() + "px";
  } 
}
 

//document.addEventListener('keydown', logKey);
function logKey(e) {
  //alert(e.code);
  if (e.code == 'KeyD' && !lockedD){
    //move char to right.
    document.getElementById('Player').style.left = (parseInt(document.getElementById('Player').style.left)  + 15).valueOf() + "px";
  }
  if (e.code == 'KeyA' && !lockedA){
    //move char to right.
    document.getElementById('Player').style.left = (parseInt(document.getElementById('Player').style.left)  - 15).valueOf() + "px";
  }
  if (e.code == 'KeyW'){
    //move char to right.
    document.getElementById('Player').style.top = (parseInt(document.getElementById('Player').style.top)  - 15).valueOf() + "px";
  }
  // if (e.code == 'KeyS'){
  //   //move char to right.
  //   document.getElementById('Player').style.top = ((parseInt(document.getElementById('Player').style.top))  + 15).valueOf() + "px";
  // }
  

  

}


@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  posX: number = 400;
  posY: number = 400;

  tileWidth: number;
  tileHeight: number;

  @Input() menuMargin: number = 110;
  tileSize: number;
  constructor() { }

  ngOnInit() {

  }

  message: string;
  receiveMessage($tileWidth) {
    this.message = $tileWidth
  }

  loadPlayer($tileWidth) {
    this.tileSize = $tileWidth;
    this.posX = 2 * this.tileSize;
    this.tileWidth = this.tileSize;
    this.tileHeight = this.tileSize;

    this.posX = this.tileWidth;
    this.posY = this.tileWidth * 8 + this.menuMargin;
    // alert('asd');
  }

}
