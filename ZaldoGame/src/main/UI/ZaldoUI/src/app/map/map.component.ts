import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { TileComponent } from '../tile/tile.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  constructor() { }

  leftImage = ''
  leftTilesxx = 20;
  posY = 0;

  posX = 0;
  leftTiles: TileComponent [] = [];
  cornerTiles: TileComponent [] = [];
  roofTiles: TileComponent [] = [];

  rightTiles: TileComponent [] = [];

  floorTiles: TileComponent[] = [];

  mapSize = 8; // sets the width nad height of the level (this is the width of work area - border is +1 on each side)
  tileCountX = 55;
  tileCountY = 24;
  viewRatio = 22/8
  //menuMargin = 110;  // shared off many components. should move to app.
  tileSize = 40; //      goal is 24 tiles * 80 in size = 1920  - perfect width for monitor  (1920/1080)
  //tileWidth: 50; //used for rectangular maps
  //tileHeight: 50;
  @Output() loadPlayer = new EventEmitter();
  @Input() menuMargin: number;
  ngOnInit() {
    this.loadLevel();
    this.loadPlayer.emit(this.tileSize);
  }

  loadLevel() {
    console.log("Loading Level...");
    //maximse the 
    var w = window.innerWidth;
    var h = window.innerHeight;
    var  screenResX = screen.width;

    this.tileSize = w / screenResX * 80

    //construct the tiles, place in the with and height as specified
    var Xsize = 22;
    var Ysize = 8;
    this.tileCountX = 22;
    this.tileCountY = 8;
    var corners = 4;
    while (Xsize > 0) {
      console.log( "Block statement execution no." + Xsize)
      Xsize--;
      this.posX = this.posX + this.tileSize;
      this.addFloorTile();
      this.addRoofTile();
    }
    while (Ysize > 0) {
      console.log( "setting wall no." + Ysize)
      Ysize--;
      this.addLeftTile();
      this.addRightTile();
      this.posY = this.posY + this.tileSize;
    }
    while (corners > 0) {
      this.addCornerTiles();
      corners--;
    }
  }

  addLeftTile() {
    var newTile = new TileComponent();
    newTile.posX = 0;
    newTile.posY = this.posY + this.menuMargin + this.tileSize;
    newTile.tileHeight = this.tileSize;
    newTile.tileWidth = this.tileSize;
    this.leftTiles.push(newTile);
  }
  addFloorTile() {
    var newTile = new TileComponent();
    newTile.posY = (this.tileCountY * this.tileSize + this.tileSize) + this.menuMargin;
    newTile.posX = this.posX;
    newTile.tileHeight = this.tileSize;
    newTile.tileWidth = this.tileSize;
    this.floorTiles.push(newTile);
  }

  addRightTile() {
    var newTile = new TileComponent();
    newTile.posX = (this.tileCountX * this.tileSize + this.tileSize);
    newTile.posY = this.posY + this.menuMargin  + this.tileSize;
    newTile.tileHeight = this.tileSize;
    newTile.tileWidth = this.tileSize;
    this.rightTiles.push(newTile);
  }

  addRoofTile() {
    var newTile = new TileComponent();
    newTile.posY = this.menuMargin;
    newTile.posX = this.posX;
    newTile.tileHeight = this.tileSize;
    newTile.tileWidth = this.tileSize;
    this.roofTiles.push(newTile);
  }

  addCornerTiles() {
    var topLeftTile = new TileComponent();
    topLeftTile.posY = this.menuMargin;
    topLeftTile.posX = 0;
    topLeftTile.tileHeight = this.tileSize;
    topLeftTile.tileWidth = this.tileSize;
    this.cornerTiles.push(topLeftTile);

    var topRightTile = new TileComponent();
    topRightTile.posY = this.menuMargin;
    topRightTile.posX = (this.tileCountX * this.tileSize + this.tileSize);
    topRightTile.tileHeight = this.tileSize;
    topRightTile.tileWidth = this.tileSize;
    this.cornerTiles.push(topRightTile);

    var bottomLeftTile = new TileComponent();
    bottomLeftTile.posY = this.menuMargin + (this.tileCountY * this.tileSize + this.tileSize);
    bottomLeftTile.posX = 0;
    bottomLeftTile.tileHeight = this.tileSize;
    bottomLeftTile.tileWidth = this.tileSize;
    this.cornerTiles.push(bottomLeftTile);

    var bottomRightTile = new TileComponent();
    bottomRightTile.posY = this.menuMargin + (this.tileCountY * this.tileSize + this.tileSize);
    bottomRightTile.posX = (this.tileCountX * this.tileSize + this.tileSize);
    bottomRightTile.tileHeight = this.tileSize;
    bottomRightTile.tileWidth = this.tileSize;
    this.cornerTiles.push(bottomRightTile);
  }



}
