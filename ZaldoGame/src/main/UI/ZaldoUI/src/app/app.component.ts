import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ZaldoUI';

  message: string;
  receiveMessage($tileWidth) {
    this.message = $tileWidth
  }
}
