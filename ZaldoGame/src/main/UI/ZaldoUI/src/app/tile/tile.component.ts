import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent implements OnInit {

  constructor() { }

  posX: number = 0;
  posY: number = 0;

  tileWidth: number;
  tileHeight: number;
  ngOnInit() {
  }

}
